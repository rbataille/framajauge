<?php

namespace Framajauge\Tests\Scraper;

use Framajauge\Scraper\AbstractScraper;

class AbstractScraperMock extends AbstractScraper
{
    public $countDoRequest = 0;

    public static function getAvailableTypes()
    {
        return ['foo', 'bar'];
    }

    protected function doRequest()
    {
        $this->response = true;
        $this->countDoRequest += 1;
    }

    protected function scrapeValue($type)
    {
        return $this->countDoRequest;
    }
}
