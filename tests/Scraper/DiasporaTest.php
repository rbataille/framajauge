<?php

namespace Framajauge\Tests\Scraper;

use Framajauge\Scraper\Diaspora;

/**
 * @todo Test with several versions and pods of Diaspora.
 */
class DiasporaTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException        \Framajauge\Exception\RequestFailed
     * @expectedExceptionMessage No Diaspora interaction found at https://framasphere.org
     */
    public function testBadPostUrlThrowsException()
    {
        $scraper = new Diaspora('https://framasphere.org');
        $scraper->get('like');
    }

    public function testReturnsNumericValue()
    {
        $scraper = new Diaspora('https://framasphere.org/posts/214044');

        $this->assertTrue(is_numeric($scraper->get('comment')));
        $this->assertTrue(is_numeric($scraper->get('like')));
        $this->assertTrue(is_numeric($scraper->get('share')));
    }
}
