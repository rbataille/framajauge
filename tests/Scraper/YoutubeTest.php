<?php

namespace Framajauge\Tests\Scraper;

use Framajauge\Scraper\Youtube;

class YoutubeTest extends \PHPUnit_Framework_TestCase
{
    const VIDEO_ID = 'tDjkoY5_Kgc';

    /**
     * @expectedException \Framajauge\Exception\RequestFailed
     */
    public function testBadVideoIdThrowsException()
    {
        $scraper = new Youtube('a_bad_video_id');
        $scraper->get('view');
    }

    public function testReturnsNumericValue()
    {
        $scraper = new Youtube(self::VIDEO_ID);

        $this->assertTrue(is_numeric($scraper->get('comment')));
        $this->assertTrue(is_numeric($scraper->get('dislike')));
        $this->assertTrue(is_numeric($scraper->get('like')));
        $this->assertTrue(is_numeric($scraper->get('view')));
    }
}
