<?php

namespace Framajauge\Tests\Scraper;

/**
 * @todo Test that `response` property is populatd depending on `Content-Type` header.
 */
class AbstractScraperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \Framajauge\Exception\UnavailableType
     */
    public function testBadTypeThrowsException()
    {
        $scraper = new AbstractScraperMock();
        $scraper->get('a_bad_type');
    }

    public function testRequestIsDoneOnce()
    {
        $scraper = new AbstractScraperMock();

        $this->assertEquals(0, $scraper->countDoRequest);
        $this->assertEquals(1, $scraper->get('foo'));
        $this->assertEquals(1, $scraper->get('bar'));
    }
}
