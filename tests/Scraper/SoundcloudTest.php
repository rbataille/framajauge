<?php

namespace Framajauge\Tests\Scraper;

use Framajauge\Scraper\Soundcloud;

class SoundcloudTest extends \PHPUnit_Framework_TestCase
{
    // These 2 links correspond to the same track
    const VALID_URL_HUMAN_FORM = 'https://soundcloud.com/teamsesh/bones-3m';
    const VALID_URL_API_FORM = 'https://api.soundcloud.com/tracks/179587651';

    // Url of a track without public statistic
    const URL_OF_TRACK_WITHOUT_STAT = 'https://soundcloud.com/raesremmurd/rae-sremmurd-no-type-prod-by-mike-will-made-it-swae-lee-of-rae-sremmurd';

    // @todo: take the client ID of the application.
    // Currently, this client ID is owned by freepius!
    const VALID_CLIENT_ID = 'bf262d0da753db6631e26cc274f7cd48';

    /**
     * @expectedException \Framajauge\Exception\RequestFailed
     */
    public function testBadClientIdThrowsException()
    {
        $scraper = new Soundcloud(self::VALID_URL_HUMAN_FORM, 'a_bad_client_id');
        $scraper->get('playback');
    }

    /**
     * @expectedException \Framajauge\Exception\RequestFailed
     */
    public function testBadUrlThrowsException()
    {
        $scraper = new Soundcloud('a_bad_url', self::VALID_CLIENT_ID);
        $scraper->get('playback');
    }

    /**
     * @expectedException \Framajauge\Exception\UnscrapedValue
     */
    public function testTrackWithoutStatThrowsException()
    {
        $scraper = new Soundcloud(self::URL_OF_TRACK_WITHOUT_STAT, self::VALID_CLIENT_ID);
        $scraper->get('playback');
    }

    public function testReturnsNumericValue()
    {
        $scraper = new Soundcloud(self::VALID_URL_HUMAN_FORM, self::VALID_CLIENT_ID);

        $this->assertTrue(is_numeric($scraper->get('comment')));
        $this->assertTrue(is_numeric($scraper->get('download')));
        $this->assertTrue(is_numeric($scraper->get('like')));
        $this->assertTrue(is_numeric($scraper->get('playback')));
    }

    public function testHumanAndApiUrlsReturnSameValues()
    {
        $scHuman = new Soundcloud(self::VALID_URL_HUMAN_FORM, self::VALID_CLIENT_ID);
        $scApi   = new Soundcloud(self::VALID_URL_API_FORM  , self::VALID_CLIENT_ID);

        foreach (Soundcloud::getAvailableTypes() as $type) {
            $this->assertEquals($scHuman->get($type), $scApi->get($type));
        }
    }
}
