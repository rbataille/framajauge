FramaJauge
==========

A way to measure the Web attention.

Requirements
------------

To run the **FramaJauge** project, you need:

* to configure a web server
* PHP 5.4
* Git

Installation
------------

Firstly, clone the **FramaJauge** repository:
``git clone https://git.framasoft.org/rbataille/framajauge.git``

Secondly, install [Composer] (the most popular dependency manager for PHP):
``php -r "readfile('https://getcomposer.org/installer');" | php``

Finally, install the project dependencies: ``php composer.phar install``

Tests
-----

To run the test suite, you need :

1. to install the *development* dependencies: ``php composer.phar install --dev``
1. to execute [PHPUnit]: ``vendor/bin/phpunit`` or ``vendor/bin/phpunit tests/My/Particular/Test.php``

License
-------

The **FramaJauge** is licensed under the **???** license.


[Composer]: http://getcomposer.org
[PHPUnit]: https://phpunit.de
