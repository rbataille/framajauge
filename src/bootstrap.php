<?php

define('APP' , __DIR__);
define('ROOT', dirname(APP));

$loader = require ROOT.'/vendor/autoload.php';

/*
 * Include host-dependent configuration parameters
 * (with servernames, passwords...).
 */
require_once APP.'/load-config.php';

$app = new \Silex\Application();

/* Locale of the current request : default = fr */
$app['locale'] = 'fr';

/* debug */
$app['debug'] = DEBUG;

/* MongoDB config */
$app['mongo.connection'] = new \MongoClient(MONGO_SERVER);
$app['mongo.database'] = $app['mongo.connection']->selectDB(MONGO_DB);

/* Paths and directories */
$app['path.cache'] = ROOT.'/cache';
$app['path.web']   = ROOT.'/web';


/*************************************************
 * Register services
 ************************************************/

/* session */
$app->register(new \Silex\Provider\SessionServiceProvider());

/* twig */
$app->register(new \Silex\Provider\TwigServiceProvider(), [
    'twig.path' => [APP.'/Resources/views'],
    'twig.options' => ['cache' => DEBUG ? null : ($app['path.cache'].'/twig')],
]);

/* validator */
$app->register(new \Silex\Provider\ValidatorServiceProvider());

/* locale */
$app->register(new \Silex\Provider\LocaleServiceProvider());

/* translator */
$app->register(new \Silex\Provider\TranslationServiceProvider());

/* security */
//$app->register(new \Silex\Provider\SecurityServiceProvider());


/*************************************************
 * Add translation resources
 ************************************************/

$translator = $app['translator'];
$transDir   = APP.'/Resources/translations';
$locales    = ['fr', 'en'];
$resources  = ['messages'];

foreach ($locales as $locale) {
    foreach ($resources as $resource) {
        $translator->addResource('array', require_once "$transDir/$resource.$locale.php", $locale);
    }
}

// TODO: Bug! Actually, calling *translator* service sets to null the 'locale' parameter.
// The Bug is declared on GitHub #982. Follow it!
$app['locale'] = 'fr';


/*************************************************
 * Register repositories
 ************************************************/

//$app['model.repository.blog'] = function ($app)
//{
    //return new \Framajauge\Model\Repository\Blog($app['mongo.database']->blog);
//};


/*************************************************
 * Register entity factories
 ************************************************/

//$app['model.factory.article'] = function ($app)
//{
    //return new \Framajauge\Model\Factory\Article($app['validator'], $app['model.repository.blog']);
//};


/*************************************************
 * Define the routes
 ************************************************/

$app->mount('/', new \Framajauge\Controller\BaseController($app));


return $app;
