<?php

namespace Framajauge\Scraper;

use Framajauge\Exception\UnscrapedValue;

class Soundcloud extends AbstractScraper
{
    /**
     * @link https://developers.soundcloud.com/docs/api/reference#resolve
     */
    const API_URL_PATTERN = "http://api.soundcloud.com/resolve.json?url=%s&client_id=%s";

    /**
     * Mapping between the available types and the SoundCloud types.
     */
    const SOUNDCLOUD_TYPES = [
        'comment'  => 'comment_count',
        'download' => 'download_count',
        'like'     => 'favoritings_count',
        'playback' => 'playback_count',
    ];

    /**
     * @param string $trackUrl  The url of a SoundCloud track.\
     *                          Eg: https://soundcloud.com/teamsesh/bones-3m or \
     *                              https://api.soundcloud.com/tracks/162777996
     *
     * @param string $clientId  The client ID of a SoundCloud registered app.\
     *                          See {@link http://soundcloud.com/you/apps}
     */
    public function __construct($trackUrl, $clientId)
    {
        $this->url = sprintf(self::API_URL_PATTERN, $trackUrl, $clientId);
    }

    public static function getAvailableTypes()
    {
        return ['comment', 'download', 'like', 'playback'];
    }

    protected function scrapeValue($type)
    {
        $soundcloudType = self::SOUNDCLOUD_TYPES[$type];

        if (array_key_exists($soundcloudType, $this->response)) {
            return $this->response[$soundcloudType];
        }

        throw new UnscrapedValue('Soundcloud', $type);
    }
}
