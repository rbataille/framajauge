<?php

namespace Framajauge\Scraper;

interface ScraperInterface
{
    /**
     * Scrape the value of a given type.
     *
     * @param string $type
     *
     * @throws \Framajauge\Exception\RequestFailed    When the http request fails
     * @throws \Framajauge\Exception\UnavailableType  When the type is not available
     * @throws \Framajauge\Exception\UnscrapedValue   When the value can't be scraped from the http response
     *
     * @return integer
     */
    public function get($type);
}
