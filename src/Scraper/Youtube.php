<?php

namespace Framajauge\Scraper;

use Framajauge\Exception\RequestFailed;
use Framajauge\Exception\UnscrapedValue;

class Youtube extends AbstractScraper
{
    /**
     * @link https://developers.google.com/youtube/2.0/reference
     *
     * Actually, we use the obsolete YouTube API 2.0.
     * The v3 is probably more complete, but harder to use.
     */
    const API_URL_PATTERN = "https://gdata.youtube.com/feeds/api/videos/%s?v=2&alt=json";

    /**
     * @param string $videoId   The id of a YouTube video
     */
    public function __construct($videoId)
    {
        $this->url = sprintf(self::API_URL_PATTERN, $videoId);
    }

    public static function getAvailableTypes()
    {
        return ['comment', 'dislike', 'like', 'view'];
    }

    protected function doRequest()
    {
        parent::doRequest();

        if (! isset($this->response['entry'])) {
            throw new RequestFailed("No YouTube video at {$this->url}");
        }
    }

    protected function scrapeValue($type)
    {
        $entry = $this->response['entry'];

        if ('comment' === $type) {
            $value =& $entry['gd$comments']['gd$feedLink']['countHint'];
        }
        elseif ('dislike' === $type) {
            $value =& $entry['yt$rating']['numDislikes'];
        }
        elseif ('like' === $type) {
            $value =& $entry['yt$rating']['numLikes'];
        }
        elseif ('view' === $type) {
            $value =& $entry['yt$statistics']['viewCount'];
        }

        if (null !== $value) { return $value; }

        throw new UnscrapedValue('Youtube', $type);
    }
}
