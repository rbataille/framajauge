<?php

namespace Framajauge\Scraper;

use Framajauge\Exception\RequestFailed;
use Framajauge\Exception\UnavailableType;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

abstract class AbstractScraper implements ScraperInterface
{
    /**
     * @var string The full url to request
     */
    protected $url;

    /**
     * @var mixed The returned (and eventually transformed) http response
     * */
    protected $response;

    /**
     * {@inheritdoc}
     */
    public function get($type)
    {
        // Is $type available?
        if (false === in_array($type, $this->getAvailableTypes()))
        {
            throw new UnavailableType(__CLASS__, $type);
        }

        // Do *only once* the http request
        if (! $this->response) {
            $this->doRequest();
        }

        // @todo Should we throw an exception if returned value isn't a numeric?
        return $this->scrapeValue($type);
    }

    /**
     * @return array The available value types (as an array of strings)
     */
    abstract public static function getAvailableTypes();

    /**
     * Do the http request with the *Guzzle Http Client*.
     * Then, populate the `response` property, depending on `Content-Type' header.
     */
    protected function doRequest()
    {
        try {
            $response = (new Client)->get($this->url);

            strtok($response->getHeader('Content-Type'), '/');
            $contentType = strtok(';');

            $this->response =  'json' === $contentType ? $response->json() :
                              ('xml'  === $contentType ? $response->xml()  :
                                                         $response->getBody());
        }
        catch (ClientException $e) {
            throw new RequestFailed($e->getMessage());
        }
    }

    /**
     * Here dude, scrape your fucking value as you like! Hum...
     * Don't forget to return an `integer` and,
     * if necessary, to throw an `UnscrapedValue` exception.
     */
    abstract protected function scrapeValue($type);
}
