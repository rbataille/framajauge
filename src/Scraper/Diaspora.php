<?php

namespace Framajauge\Scraper;

use Framajauge\Exception\RequestFailed;
use Framajauge\Exception\UnscrapedValue;

class Diaspora extends AbstractScraper
{
    /**
     * @param string $postUrl  The url of a Diaspora post.\
     *                         Eg: https://framasphere.org/posts/214044
     */
    public function __construct($postUrl)
    {
        $this->url = $postUrl;
    }

    public static function getAvailableTypes()
    {
        return ['comment', 'like', 'share'];
    }

    protected function doRequest()
    {
        parent::doRequest();

        if (preg_match('/"interactions":({.*})};\n/', $this->response, $matches)) {
            $this->response = json_decode($matches[1], true);
        }
        else {
            throw new RequestFailed("No Diaspora interaction found at {$this->url}");
        }
    }

    protected function scrapeValue($type)
    {
        if     ('comment' === $type) { $value =& $this->response['comments_count']; }
        elseif ('like'    === $type) { $value =& $this->response['likes_count']; }
        elseif ('share'   === $type) { $value =& $this->response['reshares_count']; }

        if (null !== $value) { return $value; }

        throw new UnscrapedValue('Diaspora', $type);
    }
}
