<?php

namespace Framajauge\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Summary :
 *  -> __construct
 *  -> connect
 *  -> home
 */
class BaseController implements ControllerProviderInterface
{
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function connect(Application $app)
    {
        $ctrl = $app['controllers_factory'];

        // Home
        $ctrl->get('/', [$this, 'home']);

        return $ctrl;
    }

    public function home(Request $request)
    {
        return $this->app['twig']->render('base/home.html.twig');
    }
}
