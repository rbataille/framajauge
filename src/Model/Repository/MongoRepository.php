<?php

namespace Framajauge\Model\Repository;

use Framajauge\Exception\EntityNotFound;

/**
 * Summary :
 *  -> __construct
 *  -> getCollection
 *  -> init                     [protected]
 *  -> throwNotFoundException   [protected]
 *  -> getById
 *  -> deleteById
 *  -> store
 */
abstract class MongoRepository
{
    protected $collection;

    public function __construct(\MongoCollection $collection)
    {
        $this->collection = $collection;
        $this->init();
    }

    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Various initializations before to use the collection.
     * Examples:
     *  -> create indexes
     *  -> garbage collection
     *  -> etc.
     */
    protected function init() { }

    /**
     * Throw an exception of type 'Not found'.
     */
    protected function throwNotFoundException($message)
    {
        throw new EntityNotFound($message);
    }

    /**
     * Retrieve an entity from its id.
     * Throw an exception if no match.
     */
    public function getById($id)
    {
        $id = new \MongoId($id);

        $entity = $this->collection->findOne(['_id' => $id]);

        if (! is_array($entity)) {
            throwNotFoundException("id = $id");
        }

        return $entity;
    }

    /**
     * Delete an entity from its id.
     * Return true if the operation succeed ; false, else.
     */
    public function deleteById($id)
    {
        $id = new \MongoId($id);

        $result = $this->collection->remove(['_id' => $id]);

        return $result['err'] === null;
    }

    /**
     * Store an entity : if alreay exists, update it ; else, create it.
     * $entity may be changed after create/update operation.
     * Finally, return true if the operation succeed ; false, else.
     */
    public function store(array & $entity)
    {
        // update
        if ($id = @ $entity['_id'])
        {
            unset($entity['_id']);

            $result = $this->collection->update(['_id' => $id], ['$set' => $entity]);
        }
        // create
        else {
            $entity['_id'] = new \MongoId();

            $result = $this->collection->insert($entity);
        }

        return $result['err'] === null;
    }
}
