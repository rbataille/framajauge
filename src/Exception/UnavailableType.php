<?php

namespace Framajauge\Exception;

class UnavailableType extends \Exception
{
    public function __construct($scraper, $type)
    {
        parent::__construct(
            sprintf('The type "%s" is not valid for the scraper "%s".', $type, $scraper)
        );
    }
}
