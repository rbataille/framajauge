<?php

namespace Framajauge\Exception;

class UnscrapedValue extends \Exception
{
    public function __construct($scraper, $type)
    {
        parent::__construct(
            sprintf('The value for type "%s" and scraper "%s" can\'t be scraped.', $type, $scraper)
        );
    }
}
